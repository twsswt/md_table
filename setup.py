# -*- coding: utf-8 -*-


from setuptools import setup, find_packages


with open('markdown_table/man/VERSION', 'rb') as f:
    version = f.read().decode('utf-8')

with open('README.md', 'rb') as f:
    long_descr = f.read().decode('utf-8')

setup(
    name='markdown_table',
    packages=find_packages(),
    tests_require=['pytest', ],
    setup_requires=['pytest', 'pytest-runner'],
    zip_safe=False,
    include_package_data=True,
    version=version,
    description='A library for formatting data as a markdown table.',
    long_description=long_descr,
    author='Tim Storer',
    author_email='timothy.storer@glasgow.ac.uk',
    url='https://gitlab.com/twsswt/md_table',
)
