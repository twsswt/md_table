"""
Utility for printing a list of python Tuples as a markdown table.
"""

from .classes import MarkdownTable

from io import StringIO


def markdown_table(field_labels, table_data, alignments=None):

    with StringIO() as string_handle:
        table = MarkdownTable(field_labels, table_data, alignments)
        table.write(string_handle)

    return string_handle.getvalue()
