

def get_column_widths(table_data):
    column_widths = list()

    for col in range(0, len(table_data[0])):
        column_widths.append((max(map(lambda dr: len(str(dr[col])), table_data))))

    return column_widths


def make_format_string(column_widths, alignments):

    if len(column_widths) < 1:
        return ""

    format_string = "|"

    for column_width, alignment in zip(column_widths, alignments):
        if alignment is "l":
            format_string += " %-" + str(column_width) + "s |"
        elif alignment is "r":
            format_string += " %" + str(column_width) + "s |"
        else:
            format_string += " %-" + str(column_width) + "s |"

    return format_string + '\n'


def make_header_separator(column_widths, alignments):

    column_formats = list()
    for column_width, alignment in zip(column_widths, alignments):
        column_format = "-" * column_width
        if alignment is "l":
            column_format = ":" + column_format + "-"
        elif alignment is "r":
            column_format = "-" + column_format + ":"
        elif alignment is "c":
            column_format = ":" + column_format + ":"
        else:
            column_format = "-" + column_format + "-"

        column_formats.append(column_format)

    return "|" + "|". join(column_formats) + "|\n"


class MarkdownTable:

    def __init__(self, field_labels, data=None, alignments=None):
        self.field_labels = field_labels
        self.data = list() if data is None else data

        self.unwritten_lines = list()

        column_widths = get_column_widths([self.field_labels] + self.data)
        self.alignments = ["l"] * len(column_widths) if alignments is None else alignments

        format_string = make_format_string(column_widths, self.alignments)

        self.unwritten_lines.append(format_string % field_labels)
        self.unwritten_lines.append(make_header_separator(column_widths, self.alignments))

        for row_data in self.data:
            self.unwritten_lines.append(format_string % row_data)

    def append_row(self, row_data):

        self.data.append(row_data)
        column_widths = get_column_widths(self.data)
        format_string = make_format_string(column_widths, self.alignments)

        self.unwritten_lines.append(format_string % row_data)

    def write(self, file_handle):
        while len(self.unwritten_lines) > 0:
            file_handle.write(self.unwritten_lines.pop(0))
